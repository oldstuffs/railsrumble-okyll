# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :admin do
    email "ichunlea@me.com"
    password "Password"
  end
  factory :user do
    email "test@okyll.com"
    password "Password"
  end
end
