module Liquid
  class DatabaseSystem
    attr_reader :site

    def initialize(site)
      @site = site
    end

    # Return a valid liquid template string
    def read_template_file(template_path, context)
      @template = site.templates.where(name: template_path).first
      raise FileSystemError, "No such template '#{template_path}'" if @template.nil?
      @template.body unless @template.nil?
    end
  end
end
