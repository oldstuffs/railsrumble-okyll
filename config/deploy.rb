set :application, "okyll"
set :repo_url, 'git@github.com:railsrumble/r13-team-548.git'

# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }
set :branch, 'master'

# set :deploy_to, '/var/www/my_app'
# set :scm, :git

# set :format, :pretty
# set :log_level, :debug
# set :pty, true

set :linked_files, %w{config/config.yml config/mongoid.yml config/newrelic.yml config/secret.key}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# set :default_env, { path: "/opt/ruby/bin:$PATH" }
# set :keep_releases, 5
set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, '2.0.0-p247'

namespace :deploy do
  
  desc 'Updated'
  task :updated do
    puts "Updated"
  end
  desc 'Reverted'
  task :reverted do
    puts "Reverted"
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5, :except => { :no_release => true } do
      # execute "cd #{current_path} && RAILS_ENV=production bundle exec unicorn_rails -c #{fetch(:unicorn_config)} -D"
      execute "if [ -f #{fetch(:unicorn_pid)} ]; then kill -s USR2 `cat #{fetch(:unicorn_pid)}`; fi"
    end
  end

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within current_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  after :finishing, 'deploy:cleanup'

end

namespace :unicorn do

  task :start do
    on roles(:app), in: :sequence, wait: 5, :except => { :no_release => true } do
      within current_path do
        # BUG: Something wrong with this so I need to run "RAILS_ENV=production bundle exec unicorn_rails -c /home/okyll/okyll-production/current/config/unicorn.rb -D -E production" on VPS by myself
        # puts fetch(:rails_env)
        execute :bundle, :exec, :unicorn_rails, "-c #{fetch(:unicorn_config)} -D -E #{fetch(:rails_env, 'production')}"
      end
    end
  end

  task :stop do
    on roles(:app), in: :sequence, wait: 5, :except => { :no_release => true } do
      execute "if [ -f #{fetch(:unicorn_pid)} ]; then kill -QUIT `cat #{fetch(:unicorn_pid)}`; fi"
    end
  end
  
  task :restart do
    on roles(:app), in: :sequence, wait: 5, :except => { :no_release => true } do
      # execute "cd #{current_path} && RAILS_ENV=production bundle exec unicorn_rails -c #{fetch(:unicorn_config)} -D"
      execute "if [ -f #{fetch(:unicorn_pid)} ]; then kill -s USR2 `cat #{fetch(:unicorn_pid)}`; fi"
    end
  end
  
end
