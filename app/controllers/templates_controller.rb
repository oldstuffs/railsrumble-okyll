class TemplatesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_site
  before_action :set_template, only: [:show, :edit, :update, :destroy]
  
  layout "site_dashboard"
  
  def index
    @templates = @site.templates.page params[:page]
  end
  
  def show
  end

  def new
    @template = Template.new
  end

  def edit
  end

  def create
    @template = @site.templates.new(template_params)

    respond_to do |format|
      if @template.save
        format.html { redirect_to site_template_path(@site, @template), notice: 'Template was successfully created.' }
        format.json { render action: 'show', status: :created, location: [@site, @template] }
      else
        format.html { render action: 'new' }
        format.json { render json: @template.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bags/1
  # PATCH/PUT /bags/1.json
  def update
    respond_to do |format|
      if @template.update(template_params)
        format.html { redirect_to [@site, @template], notice: 'Bag was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @template.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bags/1
  # DELETE /bags/1.json
  def destroy
    @template.destroy
    respond_to do |format|
      format.html { redirect_to site_templates_path(@site) }
      format.json { head :no_content }
    end
  end
  
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_site
      @site = Site.find(params[:site_id])
    end
    
    def set_template
      @template = Template.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def template_params
      params.require(:template).permit(:name, :body, :default)
    end
    
end
