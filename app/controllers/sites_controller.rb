class SitesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_site, except: [:index, :new, :create, :new_git, :new_upload]
  
  layout "site_dashboard", only: [:show]
  
  def index
    redirect_to dashboard_url
  end

  def show
    @pages = @site.pages.page params[:page]
  end
  
  def preview
    @link = params[:link]
    unless @link.nil?
      @page = @site.pages.find_by(permalink: @link)
    else
      @page = @site.pages.homepage.first || @site.pages.first
    end
    unless @page.template.nil?
      template = @page.template.body
    else
      @template = @site.templates.default.first
      template = (@template.body unless @template.nil?) || "{{content}}"
    end
    Liquid::Template.file_system = Liquid::DatabaseSystem.new(@site)
    html = Liquid::Template.parse(template).render('content' => @page.body, 'page.title' => @page.title)
    render text: html
  end

  def deploy
    case params[:way]
    when 'git'
      Deploy::Ghpages.new(@site).deploy
    when 's3'
      Deploy::S3.new(@site).deploy
    end
    # task = DeployTask.create(
    #   :site    => @site,
    #   :way   => params[:way]
    # )
    # DeployTask.delay.perform_task(task.id.to_s)
    # 
    redirect_to site_path(@site), notice: "You press deploy via #{params[:way]} for <b>#{@site.name}</b>"
  end

  def download
    html = "You press download for <b>#{@site.name}</b><br>"
    render text: html
  end

  def settings
  end
  
  def new
    @site = Site.new
  end
  
  def new_upload
    
  end
  
  def new_git
    
  end
  
  # POST /bags
  # POST /bags.json
  def create
    @site = current_user.sites.new(site_params)

    respond_to do |format|
      if @site.save
        format.html { redirect_to site_path(@site), notice: 'Site was successfully created.' }
        format.json { render action: 'show', status: :created, location: @site }
      else
        format.html { render action: 'new' }
        format.json { render json: @site.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /bags/1
  # DELETE /bags/1.json
  def destroy
    @site.destroy
    respond_to do |format|
      format.html { redirect_to dashboard_url }
      format.json { head :no_content }
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_site
      @site = Site.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def site_params
      params.require(:site).permit(:name)
    end
end
