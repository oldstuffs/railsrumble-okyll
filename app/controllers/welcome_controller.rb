class WelcomeController < ApplicationController
  layout "homepage", only: :index
  def index
    redirect_to dashboard_url if user_signed_in?
  end
  
  def login_as_try
    sign_in(:user, User.tester.first) unless user_signed_in?
    redirect_to root_url # or user_root_url
  end
  def signup_after_try
    if current_user and current_user.tester?
      sign_out(current_user)
      redirect_to new_user_registration_path # or user_root_url
    else
      redirect_to root_url
    end
  end
end
