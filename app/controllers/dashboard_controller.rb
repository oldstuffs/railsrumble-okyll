class DashboardController < ApplicationController
  before_action :authenticate_user!
  
  def index
    @sites = {}
    @sites[:own] = current_user.sites
    @sites[:designer_of] = current_user.designer_of
    @sites[:editor_of] = current_user.editor_of
    @sites[:contributor_of] = current_user.contributor_of
  end
end
