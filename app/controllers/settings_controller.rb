class SettingsController < ApplicationController
  before_action :set_site
  before_action :set_page, only: [:show, :edit, :update, :destroy]
  
  layout "site_dashboard"
  
  def index
    
  end
  
  private
  # Use callbacks to share common setup or constraints between actions.
  def set_site
    @site = Site.find(params[:id])
  end
  
end
