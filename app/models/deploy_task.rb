class DeployTask
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :way
  field :status
  
  scope :success, -> { where(:status => 'success') }
  
  belongs_to :site
  
  after_destroy do
    FileUtils.rm_r output_path, :force => true
  end

  def self.perform_task(id)
    self.find(id).deploy
  end

  def self.delete_task(id)
    self.where(:id => id).first.try(:destroy)
  end

  def tmp_path
    "#{Rails.root}/tmp/export_tasks/#{id}"
  end

  def output_path
    "#{Rails.root}/data/export_tasks/#{id}"
  end

  def success?
    status == 'success'
  end

  def error?
    status == 'error'
  end

  def deploy
    case way
    when 'git'
      Deploy::Ghpages.new(@site).deploy
    when 's3'
      Deploy::S3.new(@site).deploy
    end
    
    update_attribute :status, 'success'

    DeployTask.delay_for(1.day).delete_task(id)
  rescue => e
    update_attribute :status, 'error'
    raise e
  end

end
