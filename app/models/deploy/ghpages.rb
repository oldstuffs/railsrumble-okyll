class Deploy::Ghpages < Deploy::Base
  # okyll
  # Access Key ID:  AKIAJNMVLSXSVCPCTMOA
  # Secret Access Key:  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
  def deploy
    prepare
    
    remote = "git@github.com:chunlea/idnspod.git"
    branch = "gh-pages"
    
    puts "## Deploying via git to remote=\"#{remote}\" and branch=\"#{branch}\""
     

    pages.each do |page|
      
      puts page.permalink
      my_mkdir(File.dirname("#{tmp_path}/#{page.permalink}"))
      
      unless page.template
        template = page.site.templates.first
      else
        template = page.template
      end
      
      html = Liquid::Template.parse(template.body).render('page_content' => page.body, 'page_title' => page.title)
      
      
      File.open("#{tmp_path}/#{page.permalink}", 'w') do |f|
        f.write html
      end
        
    end
    
     Dir.chdir("#{tmp_path}") do
       unless File.exists?('.git')
         `git init`
         `git remote add origin #{remote}`
       else
         #check if the remote repo has changed
         unless remote == `git config --get remote.origin.url`.chop
           `git remote rm origin`
           `git remote add origin #{remote}`
         end
       end
    
       #if there is a branch with that name, switch to it, otherwise create a new one and switch to it
       if `git branch`.split("\n").any? { |b| b =~ /#{branch}/i }
         `git checkout #{branch}`
       else
         `git checkout -b #{branch}`
       end
    
       `git add -A`
       # '"message"' double quotes to fix windows issue
       `git commit --allow-empty -am '"Automated commit at #{Time.now.utc} by Okyll"'`
       `git push -f origin #{branch}`
     end
  end
end
