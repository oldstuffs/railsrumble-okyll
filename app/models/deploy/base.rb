class Deploy::Base
  attr_accessor :site, :tmp_path, :output_path
  
  def initialize(site, options = {})
    self.site = site
    time = Time.now.to_i
    self.tmp_path = options[:tmp_path] || "#{Rails.root}/tmp/deploy/#{site.id}/#{time}"
    # self.output_path = options[:output_path] || "#{Rails.root}/tmp/deploy_output/#{site.id}/#{time}"
    Liquid::Template.file_system = Liquid::DatabaseSystem.new(site)
  end

  def prepare
    FileUtils.mkdir_p tmp_path
    # FileUtils.mkdir_p output_path
  end

  def clean
    FileUtils.rm_r tmp_path, :force => true
  end

  def pages
    site.pages.asc(:created_at)
  end

  def helper
    ApplicationController.helpers
  end

  def url_helper
    Rails.application.routes.url_helpers
  end

  def clean_body(body)
    helper.article_format_body(helper.article_remove_h1(body))
  end
  
  def deploy
    prepare
    
    pages.each do |page|
      puts page.permalink
      my_mkdir(File.dirname("#{tmp_path}/#{page.permalink}"))
      
      unless page.template
        template = page.site.templates.first
      else
        template = page.template
      end
      html = Liquid::Template.parse(template.body).render('page_content' => page.body, 'page_title' => page.title)
      
      
      File.open("#{tmp_path}/#{page.permalink}", 'w') do |f|
        f.write html
      end
    end
    # 
    # FileUtils.mkdir_p "#{tmp_path}/_posts"
    # 
    # articles.each do |article|
    #   name = article.urlname.present? ? "#{article.token}-#{article.urlname}" : article.token
    #   filename = "#{article.created_at.to_date.to_s}-#{name}.md"
    # 
    #   File.open("#{tmp_path}/_posts/#{filename}", 'w') do |f|
    #     f.write "---\n"
    #     f.write "layout: post\n"
    #     f.write "title: #{article.title}\n"
    #     f.write "published: #{article.publish?}\n"
    #     f.write "---\n\n"
    # 
    #     f.write PandocRuby.convert(clean_body(article.body), { :from => :html, :to => 'markdown+hard_line_breaks' }, 'atx-headers')
    #   end
    # end
    # 
    # FileUtils.rm "#{output_path}/jekyll.zip", :force => true
    # `cd #{tmp_path} ; zip -r #{output_path}/jekyll.zip _posts`
    # 
    # clean
  end
  
  def my_mkdir(dirPath)
    unless File.exist?(dirPath)
      my_mkdir(dirPath[0, dirPath.rindex('/')]) if dirPath.rindex('/')
      Dir::mkdir dirPath
    end
  end

end