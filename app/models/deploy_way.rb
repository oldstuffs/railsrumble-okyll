class DeployWay
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :type
  field :keys, type: Hash
  
  embedded_in :site
end
