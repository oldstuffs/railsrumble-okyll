class Authorization
  include Mongoid::Document

  field :provider
  field :uid, type: String
  embedded_in :user, :inverse_of => :authorizations

  validates_presence_of :uid, :provider
  validates_uniqueness_of :uid, :scope => :provider
end