class User
  module OmniauthCallbacks
    PROVIDER = ["github", "google", "twitter", "facebook"]

    PROVIDER.each do |provider|
      define_method "find_or_create_for_#{provider}" do |response|
        uid = response["uid"]
        data = response["info"]

        if user = User.where("authorizations.provider" => provider , "authorizations.uid" => uid).first
          user
        else
          user = User.new_from_provider_data(provider, uid, data)

          if user.save(:validate => false)
            user.authorizations << Authorization.new(:provider => provider, :uid => uid )
            return user
          else
            Rails.logger.warn("User.create_from_hash Fail，#{user.errors.inspect}")
            return nil
          end
        end
      end
    end

    def new_from_provider_data(provider, uid, data)
      User.new do |user|
        if data["email"].present? && !User.where(:email => data["email"]).exists?
          user.email = data["email"]
        else
          user.email = "#{provider}+#{uid}@okyll.com"
        end
        # user.name = data['name']
        user.password = Devise.friendly_token[0, 20]
      end
    end
  end
end