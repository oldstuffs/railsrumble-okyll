class Page
  include Mongoid::Document
  include Mongoid::Timestamps
  
  PAGETYPE = ["plain", "markdown", "html"]
  
  scope :homepage, -> { where(homepage: true) }
  
  field :title, type: String
  field :permalink, type: String
  field :published, type: Mongoid::Boolean, default: false
  field :homepage, type: Mongoid::Boolean, default: false
  field :body
  field :type, type: String
  
  belongs_to :site
  belongs_to :user
  has_one :template
  
  validates :title, presence: :true
  validates :permalink, presence: :true,uniqueness:  {
                     scope: :site,
                     :message => "should happen once only in your account" }
  validates :published, presence: :true
  validates :body, presence: :true
  validates :type, presence: :true, inclusion: PAGETYPE
end
