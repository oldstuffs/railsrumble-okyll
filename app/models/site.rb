class Site
  include Mongoid::Document
  include Mongoid::Timestamps
  
  
  field :name
  
  validates :name, confirmation: true,
                   presence: true,
                   uniqueness: {
                     scope: :owner,
                     :message => "should happen once only in your account" }
  
  belongs_to :owner, class_name: "User", inverse_of: :sites
  has_and_belongs_to_many :designers,  inverse_of: :designer_of, class_name: 'User'
  has_and_belongs_to_many :editors,  inverse_of: :editor_of, class_name: 'User'
  has_and_belongs_to_many :contributors,  inverse_of: :contributor_of, class_name: 'User'
  
  has_many :templates, dependent: :destroy
  has_many :pages, dependent: :destroy
  has_many :deploy_tasks
  
  embeds_one :deploy_way
end
