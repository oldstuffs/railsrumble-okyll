class Template
  include Mongoid::Document
  include Mongoid::Timestamps
  
  scope :default, -> { where(default: true) }
  
  field :name, :type => String
  field :body
  field :default, :type => Mongoid::Boolean, :default => false
  
  validates :name, :presence => true, uniqueness: { scope: :site,
                   :message => "should happen once only in one site" }
  validates :body, :presence => true
  
  belongs_to :site
  belongs_to :page
end
