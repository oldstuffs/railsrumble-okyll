json.array!(@bags) do |bag|
  json.extract! bag, :name, :size
  json.url bag_url(bag, format: :json)
end
