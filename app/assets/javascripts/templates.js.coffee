# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  window.codemirror_editors = {}
  $(".codemirror").each ->
    $el = $(this)
    codemirror_editors[$el.attr("id")] = CodeMirror.fromTextArea($el[0],
      mode: "text/html"
      tabMode: "indent"
      textWrapping: false
      lineNumbers: true
    )

