source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.0'

# Use Slim template
gem 'slim-rails'

# Use Cells
gem 'cells'

# Use Bootstrap 3
gem 'anjlab-bootstrap-rails', :require => 'bootstrap-rails',
                              :github => 'anjlab/bootstrap-rails'
gem 'simple_form', "~>3.0.0"
gem 'kaminari'
# Use Font-Awesome
gem 'font-awesome-rails'
gem 'bootstrap-switch-rails'

# Use Devise for User Management
gem 'devise'

# Use Cancan for Permission
gem 'cancan'

# Use OmniAuth for 3rd paltform
gem 'omniauth'
gem 'omniauth-github'
gem 'omniauth-twitter'

gem 'sidekiq'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# Use unicorn as the app server
gem 'unicorn'

# Use YAML to save settings
gem 'settingslogic'

# Use Mongoid (Add in Gemfile)
gem 'mongoid', github: 'mongoid/mongoid'

# Use Capistrano for deployment
group :development do
  gem 'capistrano',  '~> 3.0.0', require: false
  gem 'capistrano-rails', require: false
  gem 'capistrano-rbenv', github: 'capistrano/rbenv', require: false
  gem 'capistrano-bundler', require: false

  # Only for Developer
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'pry-rails'
  gem 'thin', '~> 1.6.0'
  # gem 'rails-erd' # Only for ActiveRecord
end

# For Testing
group :development, :test do
  gem 'rspec-rails', '~>2.14.0'
  # # The following optional lines are part of the advanced setup.
  gem 'guard-rspec', require: false
  gem 'spork-rails', '~> 4.0.0'
  gem 'guard-spork', '~> 1.5.1'
  gem 'childprocess', '~> 0.3.9'
  gem 'guard-livereload', '~> 2.0.0', require: false
  gem 'guard-pow', '~> 2.0.0', require: false
  gem 'guard-bundler'
  gem 'fuubar', '~> 1.2.1'
  gem 'launchy', '~> 2.3.0'
  gem 'rspec-cells', '~> 0.1.7'
  gem 'mongoid-rspec', '~> 1.9.0'
  gem 'letter_opener', '~> 1.1.2'
end

group :test do
  gem 'selenium-webdriver', '~> 2.35.1'
  gem 'capybara', '~>2.1.0'
  gem 'factory_girl_rails', '~>4.2.1' ,:require => false
  gem 'cucumber-rails', '~>1.4.0', :require => false
  gem 'database_cleaner', github: 'bmabey/database_cleaner'
  gem 'simplecov', require: false
  
  # Uncomment this line on OS X.
  gem 'growl', '~>1.0.3'
   
  # Uncomment these lines on Linux.
  # gem 'libnotify', '0.8.0'
  
  # Uncomment these lines on Windows.
  # gem 'rb-notifu', '0.0.4'
  # gem 'win32console', '1.3.2'
end

gem 'liquid'
gem 'codemirror-rails'

# for Deploy
gem 'aws-sdk'

gem 'newrelic_rpm'

# Use debugger
# gem 'debugger', group: [:development, :test]
